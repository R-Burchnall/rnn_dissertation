""" https://towardsdatascience.com/human-activity-recognition-har-tutorial-with-keras-and-core-ml-part-1-8c05e365dfa0 """
from __future__ import print_function
from matplotlib import pyplot as plt
import random

import numpy as np
import pandas as pd
import seaborn as sns
from scipy import stats
from IPython.display import display, HTML

from sklearn import metrics
from sklearn.metrics import classification_report
from sklearn import preprocessing

import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Reshape
from keras.layers import Conv2D, MaxPooling2D
from keras.utils import np_utils

from GetData import Data

data_class = Data()
DATA, LABELS = data_class.GetAllData()
print(len(LABELS))
temp = list(zip(DATA, LABELS))
random.shuffle(temp)
DATA, LABELS = zip(*temp)
print(len(LABELS))

# The number of steps within one time segment
TIME_PERIODS = 10
# The steps to take from one segment to the next; if this value is equal to
# TIME_PERIODS, then there is no overlap between the segments
STEP_DISTANCE = 3

def create_segments_and_labels(df, time_steps, step, label_name):

    # x, y, z acceleration as features
    N_FEATURES = 3
    # Number of steps to advance in each iteration (for me, it should always
    # be equal to the time_steps in order to have no overlap between segments)
    # step = time_steps
    segments = []
    labels = []
    for i in range(0, len(df)): #For every file
        xs = np.ndarray.tolist(df[0]) # X Axis
        ys = np.ndarray.tolist(df[1]) # Y Axis
        zs = np.ndarray.tolist(df[2]) # Z Axis
        # Retrieve the most often used label in this segment
        segments.append([xs, ys, zs])
        for i in range(0, len(xs)):
            labels.append(label_name[i])

    # Bring the segments into a better shape
    reshaped_segments = np.asarray(segments, dtype=np.float32).reshape(-1, time_steps, N_FEATURES)
    labels = np.asarray(labels)
    return reshaped_segments, labels

# Encode the labels as an integer
le = preprocessing.LabelEncoder()
LABELS = le.fit_transform(LABELS) 
print(LABELS)

x_train, y_train = create_segments_and_labels(DATA,
                                              TIME_PERIODS,
                                              STEP_DISTANCE,
                                              LABELS)

print('x_train shape: ', x_train.shape)
print(x_train.shape[0], 'training samples')
print('y_train shape: ', y_train.shape)

# Set input & output dimensions
num_time_periods, num_sensors = x_train.shape[1], x_train.shape[2]
num_classes = le.classes_.size
print(list(le.classes_))

input_shape = (num_time_periods*num_sensors)
x_train = x_train.reshape(x_train.shape[0], input_shape)
print('x_train shape:', x_train.shape)
print('input_shape:', input_shape)

y_train_hot = np_utils.to_categorical(y_train, num_classes)
print('New y_train shape: ', y_train_hot.shape)

model_m = keras.models.load_model("Model/Classifier.h5")

predictions = model_m.predict(x_train)
temp = y_train.tolist()
for i in range(0, len(predictions)):
    predictions_as_list = predictions.tolist()
    prediction_as_list = predictions[i].tolist()
    indexOfMax = prediction_as_list.index(max(prediction_as_list))
    print("|{}| Prediction of {} - Answer: {} ---- value of {} ".format(
            i,
            le.inverse_transform([indexOfMax]), 
            le.inverse_transform([temp[i]]),
            max(prediction_as_list)))