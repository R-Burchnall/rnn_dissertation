import os
import numpy as np
import tensorflow as tf
import tensorflow.keras as keras
import tensorflow.keras.layers as layers

from GetData import Data

# Class used to return batching values
""" GLOSSARY OF TERMS
data:       The collection of data that will be 'trained', 'tested', 'validated'
num_steps:  Number of items that will be fed into the network
batch_size: Number of elements at a time 
Skip step:  Number of words to skip over ebtween training smamples with a batch
vocabulary: Number of unique values in data set
"""
class KerasBatchGenerator(object):

    def __init__(self, data, labels, num_steps, batch_size, vocabulary, skip_step=5):
        self.data = data
        self.labels = labels
        self.num_steps = num_steps
        self.batch_size = batch_size
        self.vocabulary = vocabulary
        # this will track the progress of the batches sequentially through the
        # data set - once the data reaches the end of the data set it will reset
        # back to zero
        self.current_idx = 0
        # skip_step is the number of words which will be skipped before the next
        # batch is skimmed from the data set
        self.skip_step = skip_step

    """
    x:      The actual data
    y:      The labels corresponding to the data

    Steps...
    - populate data in batches
    - test if we need to reset current-idx, if current_idk + steps > batch_size, restart loop
    - populate temp Y with the expected outputs
    - move current_idx
    """
    def generate(self):
        x = np.zeros((self.batch_size, self.num_steps)) # 2d array to contain all of our data
        y = np.zeros((self.batch_size, self.num_steps, self.vocabulary)) # 3d array to contain outputs for labels for each potential output
        while True:
            for i in range(self.batch_size):
                if self.current_idx + self.num_steps >= len(self.data):
                    # reset the index back to the start of the data set
                    self.current_idx = 0
                x[i, :] = np.ndarray.flatten(self.data[self.current_idx:self.current_idx + self.num_steps])
                temp_y = np.ndarray.flatten(self.data[self.current_idx + 3:self.current_idx + self.num_steps + 3])
                # convert all of temp_y into a one hot representation
                y[i, :, :] = keras.to_categorical(temp_y, num_classes=self.vocabulary)
                self.current_idx += self.skip_step
            yield x, y

""" Note, for this implimentation the same data will be used to test """
def load_data(data_class):
    # Get all data as a (n, 3) array
    data, labels = data_class.GetAllData()

    # build the complete vocabulary, then convert text data to list of integers
    word_to_id = {"Accel" : 1, "Left" : 2, "Right" : 3, "Break": 4}
    train_data = data
    valid_data = data
    test_data = data
    vocabulary = len(word_to_id)
    reversed_dictionary = dict(zip(word_to_id.values(), word_to_id.keys()))

    # print(train_data[:5])
    # print(word_to_id)
    # print(vocabulary)
    return train_data, valid_data, test_data, vocabulary, reversed_dictionary, labels

def build_model(vocabulary, hidden_size, num_steps, use_dropout=True):
    model = keras.Sequential()
#    model.add(layers.Embedding(vocabulary, hidden_size, input_length=num_steps))
    model.add(layers.LSTM(hidden_size, return_sequences=True))
    model.add(layers.LSTM(hidden_size, return_sequences=True))
    if use_dropout:
        model.add(layers.Dropout(0.5))
    model.add(layers.TimeDistributed(layers.Dense(vocabulary)))
    model.add(layers.Activation('softmax'))

    # Compile the model
    model.compile(loss='categorical_cross_entropy', optimizer='adam', metrics=['accuracy'])
    return model

"""
Steps...
- Load data
- Initalise values
- Create batches
- Create model
- Compile the model
- Train
- Validate
"""
tf.logging.set_verbosity("DEBUG")
tf.enable_eager_execution()

# Load data
data_class = Data()
train_data, valid_data, test_data, vocabulary, reversed_dictionary, labels = load_data(data_class)

# Initalise values
# Data Values
vocabulary = 4
num_steps = len(train_data)//3
batch_size = 3
skip_step = 3

# NN Values
num_epochs = 50
hidden_size = 256
checkpoint_path = "./Checkpoints/"

# Creating batch generators
training_generator = KerasBatchGenerator(train_data, labels, num_steps, batch_size, vocabulary, skip_step)
validating_generator = KerasBatchGenerator(valid_data, labels, num_steps, batch_size, vocabulary, skip_step)

# Create model structure
model = build_model(vocabulary, hidden_size, num_steps)

# In order to save the progress in training, we're going to add checkpoints to the NN
checkpointer = keras.callbacks.ModelCheckpoint(filepath=checkpoint_path + '/model-{epoch:02d}.hdf5', verbose=1)
model.fit_generator(training_generator.generate(), 
                    num_steps, 
                    num_epochs,
                    validation_data=validating_generator.generate(),
                    validation_steps=num_steps, 
                    callbacks=[checkpointer])