""" Class to specifically open and return all the data from a set of files """
import os, json
import numpy as np

class Data():
    ValidEvents = ["Accel", "Break", "Left", "Right"] 
    path = "./training_data"

    def _openFiles(self):
        files = [f for f in os.listdir(self.path) if os.path.isfile(os.path.join(self.path, f))]
        return files

    def _loadData(self, filename, files):
        obj_array = []
        for f in list(filter(lambda f: filename in f, files)):
            data = json.load(open(os.path.join(self.path, f)))
            obj = [files.index(f), data]
            obj_array.append(obj)
        return obj_array

    def _removeGravity(self, files):
        errorFiles = []
        data = files
        gravity = 0
        for f in data:
            if len(f[1]["ValuesY"]) == 0:
                errorFiles.append(f)
                continue
            for value in f[1]["ValuesY"]:
                gravity += value
            gravity = gravity / len(f[1]["ValuesY"])

            for i in range(0, len(f[1]["ValuesY"])):
                f[1]["ValuesY"][i] -= gravity

            gravity = 0
        for error in errorFiles:
            data.remove(error)
        return data

    def _trimData(self, files):
        data = files
        for f in data:
            f[1]["ValuesX"] = f[1]["ValuesX"][-10:]
            f[1]["ValuesY"] = f[1]["ValuesY"][-10:]
            f[1]["ValuesZ"] = f[1]["ValuesZ"][-10:]

            while len(f[1]["ValuesX"]) < 10:
                f[1]["ValuesX"].append(f[1]["ValuesX"][-1:][0])
            while len(f[1]["ValuesY"]) < 10:
                f[1]["ValuesY"].append(f[1]["ValuesY"][-1:][0])
            while len(f[1]["ValuesZ"]) < 10:
                f[1]["ValuesZ"].append(f[1]["ValuesZ"][-1:][0])
        return data

    def _mergeData(self, files):
        data = files
        groups = []
        for f in data:
                groups.append([f[1]["ValuesX"], f[1]["ValuesY"], f[1]["ValuesZ"]])
        return np.array(groups)

    def GetData(self, EventType = "Accel"):
        if EventType not in self.ValidEvents:
            raise Exception("Attempting to load an invalid source of data: {}".format(EventType))
        files = self._openFiles()
        data = self._loadData(EventType, files)
        data = self._removeGravity(data)
        data = self._trimData(data)
        data = self._mergeData(data)
        return data

    def GetAllData(self):
        files = self._openFiles()
        data = []
        labels = []
        for value in self.ValidEvents:
            data.extend(self._loadData(value, files))
            for y in list(filter(lambda f: value in f, files)):
                labels.extend([value])
        data = self._removeGravity(data)
        data = self._trimData(data)
        data = self._mergeData(data)
        return data, labels

    def GetLabels(self):
        return self.ValidEvents